#!/usr/bin/env bash

set -e

# This script is for use in generating an updated swagger client.

usage () { echo "Usage: $0 [-h] <semvar>" 1>&2; exit 1; }

while getopts 'h' option
do
    case "$option" in
        h  ) usage; exit;;
        \? ) echo "Unknown option: -$OPTARG" >&2; exit 1;;
        :  ) echo "Missing option argument for -$OPTARG" >&2; exit 1;;
        *  ) echo "Unimplemented option: -$OPTARG" >&2; exit 1;;
    esac
done

shift $(expr $OPTIND - 1 )

if [[ -z "$1" ]];
then
    echo "No semantic version increment argument specified. Defaulting to patch."
    SEMVARPART=patch
else
    SEMVARPART=$1
fi


if [[ "$SEMVARPART" =~ ^(major|minor|patch)$ ]]; then
    echo "Bumping $SEMVARPART version number..."
else
    echo "Invalid semantic version increment provided: '$SEMVARPART'. Valid values are major, minor, or patch."
    exit 1
fi

#TODO: add command to download latest openapi.json and do nothing if it's the same as the old one


NEW_VERSION=v`bumpversion --dry-run --allow-dirty --list $SEMVARPART | grep new_version | cut -d '=' -f2`
BRANCH_NAME=update_client_to_$NEW_VERSION


echo "Creating new versioned branch $BRANCH_NAME..."
git checkout -b $BRANCH_NAME
echo "Done creating new versioned branch $BRANCH_NAME."


echo "Checking in latest openapi.json file..."
git add openapi.json
git commit -m 'Update openapi.json'
echo "Finished checking in latest openapi.json file."


echo "Bumping current $SEMVARPART version..."
bumpversion $SEMVARPART
echo "Finished bumping current $SEMVARPART version."


echo "Generating new swagger client..."
swagger-codegen generate -i openapi.json -l python -c config.json -o .
echo "Finished generating new swagger client."


echo "Adding new swagger client files..."
git add .
echo "Finished adding new swagger client files."


echo "Committing new swagger client files..."
git commit -m "Updating client to $NEW_VERSION..."
echo "Finished committing new swagger client files."


echo "Pushing new swagger client files..."
git push origin $BRANCH_NAME
echo "Finished pushing new swagger client files."


echo "Pushing new tag $NEW_VERSION..."
git push origin $NEW_VERSION
echo "Finished pushing new tag $NEW_VERSION."
