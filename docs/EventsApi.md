# events_ingestion_client.EventsApi

All URIs are relative to *https://dev-api.datakitchen.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**post_add_run_state**](EventsApi.md#post_add_run_state) | **POST** /events/v1/add-run-state | AddRunState Event
[**post_custom**](EventsApi.md#post_custom) | **POST** /events/v1/custom | Custom Event
[**post_message_log**](EventsApi.md#post_message_log) | **POST** /events/v1/message-log | MessageLog Event
[**post_metric_log**](EventsApi.md#post_metric_log) | **POST** /events/v1/metric-log | MetricLog Event
[**post_run_status**](EventsApi.md#post_run_status) | **POST** /events/v1/run-status | RunStatus Event
[**post_test_outcomes**](EventsApi.md#post_test_outcomes) | **POST** /events/v1/test-outcomes | TestOutcomes Event
[**post_test_result**](EventsApi.md#post_test_result) | **POST** /events/v1/test-results | TestResult Event

# **post_add_run_state**
> post_add_run_state(body, event_source=event_source)

AddRunState Event

Applies a custom state to an open run. Specify the name of the pipeline and run_key of the run. If no run_key is specified, the custom state is applied to the default open run for the pipeline. Run states describe the progress or results of a pipeline execution. Standard run state types are ERROR, WARNING, and INFO. Custom run states may be used to label runs with information not typically captured, such as disk usage.

### Example
```python
from __future__ import print_function
import time
import events_ingestion_client
from events_ingestion_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: SAKey
configuration = events_ingestion_client.Configuration()
configuration.api_key['ServiceAccountAuthenticationKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ServiceAccountAuthenticationKey'] = 'Bearer'

# create an instance of the API class
api_instance = events_ingestion_client.EventsApi(events_ingestion_client.ApiClient(configuration))
body = events_ingestion_client.RunStateEventApiSchema() # RunStateEventApiSchema | Data describing the event.
event_source = 'API' # str | Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. (optional) (default to API)

try:
    # AddRunState Event
    api_instance.post_add_run_state(body, event_source=event_source)
except ApiException as e:
    print("Exception when calling EventsApi->post_add_run_state: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RunStateEventApiSchema**](RunStateEventApiSchema.md)| Data describing the event. | 
 **event_source** | **str**| Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. | [optional] [default to API]

### Return type

void (empty response body)

### Authorization

[SAKey](../README.md#SAKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_custom**
> post_custom(body, event_source=event_source)

Custom Event

Generates a custom event with the value provided for the 'data' key. The Event Ingestion API does not define the behavior of this event nor validate data key values, to provide flexibility for multiple use cases. Warning - This endpoint is not intended for use by end users.

### Example
```python
from __future__ import print_function
import time
import events_ingestion_client
from events_ingestion_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: SAKey
configuration = events_ingestion_client.Configuration()
configuration.api_key['ServiceAccountAuthenticationKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ServiceAccountAuthenticationKey'] = 'Bearer'

# create an instance of the API class
api_instance = events_ingestion_client.EventsApi(events_ingestion_client.ApiClient(configuration))
body = events_ingestion_client.CustomEventApiSchema() # CustomEventApiSchema | Data describing a custom event.
event_source = 'API' # str | Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. (optional) (default to API)

try:
    # Custom Event
    api_instance.post_custom(body, event_source=event_source)
except ApiException as e:
    print("Exception when calling EventsApi->post_custom: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**CustomEventApiSchema**](CustomEventApiSchema.md)| Data describing a custom event. | 
 **event_source** | **str**| Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. | [optional] [default to API]

### Return type

void (empty response body)

### Authorization

[SAKey](../README.md#SAKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_message_log**
> post_message_log(body, event_source=event_source)

MessageLog Event

Logs a string message related to the pipeline run, and optionally related to a specific task. Post a MessageLog event to capture error, warning, or debugging messages from external tools and scripts.

### Example
```python
from __future__ import print_function
import time
import events_ingestion_client
from events_ingestion_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: SAKey
configuration = events_ingestion_client.Configuration()
configuration.api_key['ServiceAccountAuthenticationKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ServiceAccountAuthenticationKey'] = 'Bearer'

# create an instance of the API class
api_instance = events_ingestion_client.EventsApi(events_ingestion_client.ApiClient(configuration))
body = events_ingestion_client.MessageLogEventApiSchema() # MessageLogEventApiSchema | Data describing the event.
event_source = 'API' # str | Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. (optional) (default to API)

try:
    # MessageLog Event
    api_instance.post_message_log(body, event_source=event_source)
except ApiException as e:
    print("Exception when calling EventsApi->post_message_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MessageLogEventApiSchema**](MessageLogEventApiSchema.md)| Data describing the event. | 
 **event_source** | **str**| Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. | [optional] [default to API]

### Return type

void (empty response body)

### Authorization

[SAKey](../README.md#SAKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_metric_log**
> post_metric_log(body, event_source=event_source)

MetricLog Event

Logs the value of a user-defined datum of interest, such as a row count. Post a MetricLog event to track the value of a metric through a run or for comparing the value of the metric across multiple runs.

### Example
```python
from __future__ import print_function
import time
import events_ingestion_client
from events_ingestion_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: SAKey
configuration = events_ingestion_client.Configuration()
configuration.api_key['ServiceAccountAuthenticationKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ServiceAccountAuthenticationKey'] = 'Bearer'

# create an instance of the API class
api_instance = events_ingestion_client.EventsApi(events_ingestion_client.ApiClient(configuration))
body = events_ingestion_client.MetricLogApiSchema() # MetricLogApiSchema | Data describing the event.
event_source = 'API' # str | Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. (optional) (default to API)

try:
    # MetricLog Event
    api_instance.post_metric_log(body, event_source=event_source)
except ApiException as e:
    print("Exception when calling EventsApi->post_metric_log: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**MetricLogApiSchema**](MetricLogApiSchema.md)| Data describing the event. | 
 **event_source** | **str**| Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. | [optional] [default to API]

### Return type

void (empty response body)

### Authorization

[SAKey](../README.md#SAKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_run_status**
> post_run_status(body, event_source=event_source)

RunStatus Event

Changes the status of the specified task in the pipeline. Post a RunStatus event to tell the platform that a task has started or ended with a certain result.

### Example
```python
from __future__ import print_function
import time
import events_ingestion_client
from events_ingestion_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: SAKey
configuration = events_ingestion_client.Configuration()
configuration.api_key['ServiceAccountAuthenticationKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ServiceAccountAuthenticationKey'] = 'Bearer'

# create an instance of the API class
api_instance = events_ingestion_client.EventsApi(events_ingestion_client.ApiClient(configuration))
body = events_ingestion_client.RunStatusApiSchema() # RunStatusApiSchema | Data describing the event.
event_source = 'API' # str | Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. (optional) (default to API)

try:
    # RunStatus Event
    api_instance.post_run_status(body, event_source=event_source)
except ApiException as e:
    print("Exception when calling EventsApi->post_run_status: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**RunStatusApiSchema**](RunStatusApiSchema.md)| Data describing the event. | 
 **event_source** | **str**| Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. | [optional] [default to API]

### Return type

void (empty response body)

### Authorization

[SAKey](../README.md#SAKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_test_outcomes**
> post_test_outcomes(body, event_source=event_source)

TestOutcomes Event

Reports the outcomes of a test or a set of tests. Receives a list of test outcomes by test name and status (PASSED, FAILED, or WARNING). Post a TestOutcomesEvent event to send outcomes from an external tool to the Observe platform to track and report on runs and outcomes.

### Example
```python
from __future__ import print_function
import time
import events_ingestion_client
from events_ingestion_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: SAKey
configuration = events_ingestion_client.Configuration()
configuration.api_key['ServiceAccountAuthenticationKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ServiceAccountAuthenticationKey'] = 'Bearer'

# create an instance of the API class
api_instance = events_ingestion_client.EventsApi(events_ingestion_client.ApiClient(configuration))
body = events_ingestion_client.TestOutcomesApiSchema() # TestOutcomesApiSchema | Data describing the event.
event_source = 'API' # str | Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. (optional) (default to API)

try:
    # TestOutcomes Event
    api_instance.post_test_outcomes(body, event_source=event_source)
except ApiException as e:
    print("Exception when calling EventsApi->post_test_outcomes: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TestOutcomesApiSchema**](TestOutcomesApiSchema.md)| Data describing the event. | 
 **event_source** | **str**| Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. | [optional] [default to API]

### Return type

void (empty response body)

### Authorization

[SAKey](../README.md#SAKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **post_test_result**
> post_test_result(body, event_source=event_source)

TestResult Event

Reports the results of a test or a set of tests. Receives a list of test results by test name and status (PASSED, FAILED, or WARNING). Post a TestResultsEvent event to send results from an external tool to the Observe platform to track and report on runs and outcomes.

### Example
```python
from __future__ import print_function
import time
import events_ingestion_client
from events_ingestion_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: SAKey
configuration = events_ingestion_client.Configuration()
configuration.api_key['ServiceAccountAuthenticationKey'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['ServiceAccountAuthenticationKey'] = 'Bearer'

# create an instance of the API class
api_instance = events_ingestion_client.EventsApi(events_ingestion_client.ApiClient(configuration))
body = events_ingestion_client.TestResultApiSchema() # TestResultApiSchema | Data describing the event.
event_source = 'API' # str | Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. (optional) (default to API)

try:
    # TestResult Event
    api_instance.post_test_result(body, event_source=event_source)
except ApiException as e:
    print("Exception when calling EventsApi->post_test_result: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TestResultApiSchema**](TestResultApiSchema.md)| Data describing the event. | 
 **event_source** | **str**| Set the source of the event. If unset, the Event Ingestion API will assume the source of the Event is API. Warning - This parameter is not intended for use by end users. | [optional] [default to API]

### Return type

void (empty response body)

### Authorization

[SAKey](../README.md#SAKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

