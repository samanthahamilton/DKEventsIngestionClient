# TaskStatusApiSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pipeline_name** | **str** | Required. The target pipeline for the event action. | 
**task_status** | **str** | Required. The new status to be applied to the task. | 
**external_url** | **str** | A link to source information. | [optional] 
**event_timestamp** | **datetime** | An ISO8601 timestamp that describes when the event occurred. If no timezone is specified, UTC is assumed. If unset, the Events Ingestion API applies its current time to the field. | [optional] 
**metadata** | **object** | Arbitrary key-value information, supplied by the user, to apply to the event. | [optional] 
**run_tag** | **str** | The tag of the target run for the event action. This identifier is created and managed by the user. If no run_tag is specified, the event applies to the default open run for the pipeline. | [optional] 
**task_name** | **str** | Required. The target task for the event action. Must be unique within a pipeline. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

