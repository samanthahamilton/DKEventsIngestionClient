# RunStateEventSchemaRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_timestamp** | **datetime** | An ISO8601 timestamp that describes when the event occurred. If unset, the Events Ingestion API applies its current time to the field. | [optional] 
**external_url** | **str** | A link to source information. | [optional] 
**metadata** | **object** | Arbitrary key-value information, supplied by the user, to apply to the event. | [optional] 
**pipeline_name** | **str** | Required. The target pipeline for the event action. | 
**run_tag** | **str** | The tag of the target run for the event action. This identifier is created and managed by the user. If no run_tag is specified, the event applies to the default open run for the pipeline. | [optional] 
**state_description** | **str** | A description of the state change. | [optional] 
**state_label** | **str** | Required. Case insensitive. A short tag displayed in the UI. Used in combination with state_type to define the run state. | 
**state_type** | **str** | The state to apply to the open run. Used in combination with state_label to define the run state. | [optional] [default to 'INFO']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

