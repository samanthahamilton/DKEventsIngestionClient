# RunStateEventApiSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**run_key** | **str** | The key of the target run for the event action. This identifier is created and managed by the user. If no run_key is specified, the event applies to the default open run for the pipeline. | [optional] 
**pipeline_key** | **str** | Required. The target pipeline for the event action. | 
**pipeline_name** | **str** | Optional. Human readable display value for a pipeline. | [optional] 
**external_url** | **str** | A link to source information. | [optional] 
**state_type** | **str** | The state to apply to the open run. Used in combination with state_label to define the run state. | [optional] [default to 'INFO']
**metadata** | **object** | Arbitrary key-value information, supplied by the user, to apply to the event. | [optional] 
**state_description** | **str** | A description of the state change. | [optional] 
**state_label** | **str** | Required. Case insensitive. A short tag displayed in the UI. Used in combination with state_type to define the run state. | 
**event_timestamp** | **datetime** | An ISO8601 timestamp that describes when the event occurred. If no timezone is specified, UTC is assumed. If unset, the Event Ingestion API applies its current time to the field. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

