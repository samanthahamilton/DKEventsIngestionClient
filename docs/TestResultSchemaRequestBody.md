# TestResultSchemaRequestBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_timestamp** | **datetime** | An ISO8601 timestamp that describes when the event occurred. If unset, the Events Ingestion API applies its current time to the field. | [optional] 
**external_url** | **str** | A link to source information. | [optional] 
**metadata** | **object** | Arbitrary key-value information, supplied by the user, to apply to the event. | [optional] 
**pipeline_name** | **str** | Required. The target pipeline for the event action. | 
**run_tag** | **str** | The tag of the target run for the event action. This identifier is created and managed by the user. If no run_tag is specified, the event applies to the default open run for the pipeline. | [optional] 
**task_name** | **str** | The name of the task that the result is associated with. | [optional] 
**test_results** | [**list[TestReport]**](TestReport.md) | Required. A list of objects, each describing the test in name, result, and an optional description. | 
**test_suite** | **str** | The name of the test suite with the results to report. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

