# TestReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | The reported state of the named test. | 
**description** | **str** | Optional description of the test results. | [optional] 
**name** | **str** | The name of the test. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

