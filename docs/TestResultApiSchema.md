# TestResultApiSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**task_key** | **str** | Optional. The ID of the task that the result is associated with. | [optional] 
**task_name** | **str** | Optional. A human-readable display name for the task. | [optional] 
**run_key** | **str** | The key of the target run for the event action. This identifier is created and managed by the user. If no run_key is specified, the event applies to the default open run for the pipeline. | [optional] 
**test_suite** | **str** | The name of the test suite with the results to report. | 
**pipeline_key** | **str** | Required. The target pipeline for the event action. | 
**pipeline_name** | **str** | Optional. Human readable display value for a pipeline. | [optional] 
**external_url** | **str** | A link to source information. | [optional] 
**metadata** | **object** | Arbitrary key-value information, supplied by the user, to apply to the event. | [optional] 
**test_results** | [**list[TestReport]**](TestReport.md) | Required. A list of objects, each describing the test in name, result, and an optional description. | 
**event_timestamp** | **datetime** | An ISO8601 timestamp that describes when the event occurred. If no timezone is specified, UTC is assumed. If unset, the Event Ingestion API applies its current time to the field. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

