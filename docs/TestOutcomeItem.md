# TestOutcomeItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metric_value** | **float** | An numerical representation of the test metric value. To be compared with the &#x60;min_threshold&#x60; and &#x60;max_threshold&#x60; fields. | [optional] 
**start_time** | **datetime** | An ISO timestamp of when the test execution started. | [optional] 
**description** | **str** | Optional description of the test results. | [optional] 
**min_threshold** | **float** | The minimal numerical value of the test metric for the outcome to be considered within range. | [optional] 
**metadata** | **object** | Arbitrary key-value information, supplied by the user, to apply to the test outcome. | [optional] 
**end_time** | **datetime** | An ISO timestamp of when the test execution ended. | [optional] 
**name** | **str** | The name of the test. | 
**max_threshold** | **float** | The maximum numerical value of the test metric for the outcome to be considered within range. | [optional] 
**status** | **str** | The reported state of the named test. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

