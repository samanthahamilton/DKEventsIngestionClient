# CloseRunApiSchema

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**close_all** | **bool** | Optional. If true, all open runs for the pipeline will be closed. If true and a run_tag is specified, the event is rejected. | [optional] [default to False]
**pipeline_name** | **str** | Required. The target pipeline for the event action. | 
**external_url** | **str** | A link to source information. | [optional] 
**event_timestamp** | **datetime** | An ISO8601 timestamp that describes when the event occurred. If no timezone is specified, UTC is assumed. If unset, the Events Ingestion API applies its current time to the field. | [optional] 
**metadata** | **object** | Arbitrary key-value information, supplied by the user, to apply to the event. | [optional] 
**run_tag** | **str** | The tag of the target run for the event action. This identifier is created and managed by the user. If no run_tag is specified, the event applies to the default open run for the pipeline. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

